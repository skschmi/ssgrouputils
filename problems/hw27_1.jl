include("../ssgrouputils.jl")
using SSGroupUtils
import SSGroupUtils.printMatrix
import SSGroupUtils.printComplexMatrix
import SSGroupUtils.hasclosure
import SSGroupUtils.Group
import SSGroupUtils.printCayleyTable
import SSGroupUtils.multiply
import SSGroupUtils.inverse
import SSGroupUtils.conjugates
import SSGroupUtils.subgroups
import SSGroupUtils.right_coset
import SSGroupUtils.left_coset
import SSGroupUtils.printInverses
import SSGroupUtils.generate
import SSGroupUtils.printSubgroups
import SSGroupUtils.center
import SSGroupUtils.printCenter
import SSGroupUtils.printCosetsForSubgroup
import SSGroupUtils.printGeneratedFrom
import SSGroupUtils.verifyIdentity
import SSGroupUtils.generateRegularRepresentation

function isEqual(a,b)
    return a==b
end
A = [["1"    "-1"    "i"   "-i"  "j"    "-j"  "k"    "-k"];
     ["-1"   "1"     "-i"  "i"   "-j"   "j"   "-k"   "k"];
     ["i"    "-i"    "-1"  "1"   "k"    "-k"  "-j"   "j"];
     ["-i"   "i"     "1"   "-1"  "-k"   "k"   "j"    "-j"];
     ["j"    "-j"    "-k"  "k"   "-1"   "1"   "i"    "-i"];
     ["-j"   "j"     "k"   "-k"  "1"    "-1"  "-i"   "i"];
     ["k"    "-k"    "j"   "-j"  "-i"   "i"   "-1"   "1"];
     ["-k"   "k"     "-j"  "j"   "i"    "-i"  "1"    "-1"]]

self = Group("Quaternions",["1","-1","i","-i","j","-j","k","-k"],A,"1",isEqual::Function)
verifyIdentity(self)

R = generateRegularRepresentation(self)
