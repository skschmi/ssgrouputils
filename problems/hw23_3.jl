include("../ssgrouputils.jl")
using SSGroupUtils
import SSGroupUtils.printMatrix
import SSGroupUtils.printComplexMatrix
import SSGroupUtils.hasclosure
import SSGroupUtils.Group
import SSGroupUtils.printCayleyTable
import SSGroupUtils.multiply
import SSGroupUtils.inverse
import SSGroupUtils.conjugates
import SSGroupUtils.subgroups
import SSGroupUtils.right_coset
import SSGroupUtils.left_coset
import SSGroupUtils.printInverses
import SSGroupUtils.generate
import SSGroupUtils.printSubgroups
import SSGroupUtils.center
import SSGroupUtils.printCenter
import SSGroupUtils.printCosetsForSubgroup
import SSGroupUtils.printGeneratedFrom


g = [0,1,2,3,4,5]

cayley = zeros(length(g),length(g))
for i = 1:length(g)
    for j = 1:length(g)
        cayley[i,j] = mod(g[i]+g[j],length(g))
    end
end
function isEqual(a,b)
    return a == b
end
self = Group("Z"*string(length(g)),g,cayley,0,isEqual::Function)
printCayleyTable(self)
printSubgroups(self)
