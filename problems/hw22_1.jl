
include("./ssgrouputils.jl")
using SSGroupUtils
import SSGroupUtils.printMatrix
import SSGroupUtils.printComplexMatrix
import SSGroupUtils.hasclosure
import SSGroupUtils.Group
import SSGroupUtils.printCayleyTable
import SSGroupUtils.multiply
import SSGroupUtils.inverse
import SSGroupUtils.conjugates
import SSGroupUtils.subgroups
import SSGroupUtils.right_coset
import SSGroupUtils.left_coset
import SSGroupUtils.printInverses
import SSGroupUtils.generate
import SSGroupUtils.printSubgroups
import SSGroupUtils.center
import SSGroupUtils.printCenter
import SSGroupUtils.printCosetsForSubgroup
import SSGroupUtils.printGeneratedFrom

function isEqual(a,b)
    return a==b
end
A = [["e"   "b"   "a"   "a2"  "a3"  "ab"  "a2b" "a3b"];
     ["b"   "e"   "a3b" "a2b" "ab"  "a3"  "a2"  "a"];
     ["a"   "ab"  "a2"  "a3"  "e"   "a2b" "a3b" "b"];
     ["a2"  "a2b" "a3"  "e"   "a"   "a3b" "b"   "ab"];
     ["a3"  "a3b" "e"   "a"   "a2"  "b"   "ab"  "a2b"];
     ["ab"  "a"   "b"   "a3b" "a2b" "e"   "a3"  "a2"];
     ["a2b" "a2"  "ab"  "b"   "a3b" "a"   "e"   "a3"];
     ["a3b" "a3"  "a2b" "ab"  "b"   "a2"  "a"   "e"]]

self = Group("SquareSymm",["e","b","a","a2","a3","ab","a2b","a3b"],A,1,isEqual::Function)
printSubgroups(self)

printCenter(self)
printCosetsForSubgroup(self,center(self))
printGeneratedFrom(self,["a2","b"])
