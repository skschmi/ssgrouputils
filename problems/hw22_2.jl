
include("./ssgrouputils.jl")
using SSGroupUtils
import SSGroupUtils.printMatrix
import SSGroupUtils.printComplexMatrix
import SSGroupUtils.hasclosure
import SSGroupUtils.Group
import SSGroupUtils.printCayleyTable
import SSGroupUtils.multiply
import SSGroupUtils.inverse
import SSGroupUtils.conjugates
import SSGroupUtils.subgroups
import SSGroupUtils.right_coset
import SSGroupUtils.left_coset
import SSGroupUtils.printInverses
import SSGroupUtils.generate
import SSGroupUtils.printSubgroups
import SSGroupUtils.center
import SSGroupUtils.printCenter
import SSGroupUtils.printCosetsForSubgroup
import SSGroupUtils.printGeneratedFrom

function isEqual(a,b)
    result = true
    for i=1:2
        for j=1:2
            if(a[i,j] != b[i,j])
                result = false
            end
        end
    end
    return result
end

v1 = [ 1.0+0.0im   0.0+0.0im;  0.0+0.0im   1.0+0.0im]
v2 = [ 0.0+0.0im   0.0+1.0im;  0.0+1.0im   0.0+0.0im]
v3 = [ 0.0+1.0im   0.0+0.0im;  0.0+0.0im   0.0-1.0im]
v4 = [ 0.0+0.0im   1.0+0.0im; -1.0+0.0im   0.0+0.0im]
v5 = [-1.0+0.0im   0.0+0.0im;  0.0+0.0im  -1.0+0.0im]
v6 = [ 0.0+0.0im   0.0-1.0im;  0.0-1.0im   0.0+0.0im]
v7 = [ 0.0-1.0im   0.0+0.0im;  0.0+0.0im   0.0+1.0im]
v8 = [ 0.0+0.0im  -1.0+0.0im;  1.0+0.0im   0.0+0.0im]

elements = [v1,v2,v3,v4,v5,v6,v7,v8]
A = Array{Any,2}(8,8)
for i = 1:8
    for j = 1:8
        A[i,j] = elements[i]*elements[j]
    end
end

self = Group("G",elements,A,copy(v1),isEqual::Function)

for v in self.cayleytable
    println(v)
end
