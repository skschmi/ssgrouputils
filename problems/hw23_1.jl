include("../ssgrouputils.jl")
using SSGroupUtils
import SSGroupUtils.printMatrix
import SSGroupUtils.printComplexMatrix
import SSGroupUtils.hasclosure
import SSGroupUtils.Group
import SSGroupUtils.printCayleyTable
import SSGroupUtils.multiply
import SSGroupUtils.inverse
import SSGroupUtils.conjugates
import SSGroupUtils.subgroups
import SSGroupUtils.right_coset
import SSGroupUtils.left_coset
import SSGroupUtils.printInverses
import SSGroupUtils.generate
import SSGroupUtils.printSubgroups
import SSGroupUtils.center
import SSGroupUtils.printCenter
import SSGroupUtils.printCosetsForSubgroup
import SSGroupUtils.printGeneratedFrom


g = [0,1,2,3,4,5,6,7,8]

cayley = zeros(9,9)
for i = 1:9
    for j = 1:9
        cayley[i,j] = mod(g[i]+g[j],9)
    end
end
function isEqual(a,b)
    return a == b
end
self = Group("Z9",g,cayley,0,isEqual::Function)
