# Steven Schmidt
# Nov 9, 2016
# Phys 601 -- HW21

# 1. Find the conjugacy classes of the G△  group.
# 2. Find the cosets for a subgroup of order two, and the cosets for a subgroup of order three.
# 3. What do you learn from the first two parts? What patterns emerge?


include("./ssgrouputils.jl")
using SSGroupUtils
import SSGroupUtils.printMatrix
import SSGroupUtils.printComplexMatrix
import SSGroupUtils.hasclosure
import SSGroupUtils.Group
import SSGroupUtils.printCayleyTable
import SSGroupUtils.multiply
import SSGroupUtils.inverse
import SSGroupUtils.conjugates
import SSGroupUtils.subgroups
import SSGroupUtils.right_coset
import SSGroupUtils.left_coset
import SSGroupUtils.printInverses
import SSGroupUtils.generate


# Build the triangle group
P = [[1, 2, 3] [3, 1, 2] [2, 3, 1] [1, 3, 2] [2, 1, 3] [3, 2, 1]]
function applyP(i,array)
    return Array{Int64}([array[P[1,i]],array[P[2,i]],array[P[3,i]]])
end
function indexOfTriple(triple)
    for i = 1:size(P)[2]
        if(P[:,i] == triple)
            return i
        end
    end
    return 0
end
array = [1,2,3]
table = Array{Int64}(6,6)
for i = 1:6
    for j = 1:6
        table[i,j] = indexOfTriple(applyP(j,applyP(i,array)))
    end
end
function isEqual(a,b)
    if(abs(a-b) < 1e-14 )
        return true
    end
    return false
end
self = Gtri = Group("Gtri",collect(1:6),table,1,isEqual)

println("")
println("Items in the G_tri Group:")
println(self.elements_array)

println("")
println("Cayley Table for the G_tri Group:")
printCayleyTable(self)

# find the conjugacy classes of the Gtri group
println("")
println("HW 21, number 21-1, number (1):")
setOfClasses = Set()
for a=1:6
    conjug = conjugates(self,a)
    println("conjugates of ",a,":",conjug)
    push!(setOfClasses,Set(conjug))
end
println("")
println("Conjugate classes of the G_tri Group:")
for item in setOfClasses
    println(item)
end

# find the subgroups of the Gtri group
subg = subgroups(self)
println("")
println("HW 21, number 21-1, number (2):")
println("Subgroups of G_tri group: ")
for (sg,scay) in subg
    print(string(sg)*":\n")
    printMatrix(scay)
    all_r_cosets = Set()
    all_l_cosets = Set()
    for i=1:6
        r_coset = right_coset(self,sg,i)
        l_coset = left_coset(self,sg,i)
        push!(all_r_cosets,r_coset)
        push!(all_l_cosets,l_coset)
        print("Right coset (S*t) for t=",i,": ",r_coset)
        print("  Left coset (t*S) for t=",i,": ",l_coset)
        print("\n")
    end
    print("All right cosets for subgroup: ")
    println(all_r_cosets)
    print("All left cosets for subgroup:  ")
    println(all_l_cosets)
    println("")
end

println("HW 21, number 21-1, number (3):")

println(" It looks like the conjugate classes of each size are made up")
println(" of the disjoint of the elements in the subgroups of each size.")
println(" For example, for the size-3 conjugate class, it has elements 4,5,6, ")
println(" and the size-3 subgroup is made up of elements 1,2,3.")
println("  ---> What are you looking for for this problem?")



println("")
