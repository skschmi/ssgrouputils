include("../ssgrouputils.jl")
using SSGroupUtils
import SSGroupUtils.printMatrix
import SSGroupUtils.printComplexMatrix
import SSGroupUtils.hasclosure
import SSGroupUtils.Group
import SSGroupUtils.printCayleyTable
import SSGroupUtils.multiply
import SSGroupUtils.inverse
import SSGroupUtils.conjugates
import SSGroupUtils.subgroups
import SSGroupUtils.right_coset
import SSGroupUtils.left_coset
import SSGroupUtils.printInverses
import SSGroupUtils.generate
import SSGroupUtils.printSubgroups
import SSGroupUtils.center
import SSGroupUtils.printCenter
import SSGroupUtils.printCosetsForSubgroup
import SSGroupUtils.printGeneratedFrom
import SSGroupUtils.verifyIdentity
import SSGroupUtils.generateRegularRepresentation
import SSGroupUtils.cayleyTableFromPermutationGroup
import SSGroupUtils.printConjugacyClasses
import SSGroupUtils.verifyGroup
import SSGroupUtils.printConjugates
import SSGroupUtils.are_conjugate

function isEqual(a,b)
	if(length(a) != length(b))
		return false
	end
	retval = true
	for i = 1:length(a)
		if(a[i] != b[i])
			retval = false
		end
	end
    return retval
end
G = Array{Array{Int64}}(12)
G[1] = [1,2,3,4]
G[2] = [2,3,1,4]
G[3] = [3,1,2,4]
G[4] = [2,1,4,3]
G[5] = [4,3,2,1]
G[6] = [1,4,2,3]
G[7] = [1,3,4,2]
G[8] = [3,2,4,1]
G[9] = [4,2,1,3]
G[10] = [4,1,3,2]
G[11] = [2,4,3,1]
G[12] = [3,4,1,2]
A = cayleyTableFromPermutationGroup(G)

d = Dict{Any,Any}()
for i = 1:12
	d[G[i]] = i
end

B = zeros(size(A))
for i = 1:12
	for j = 1:12
		B[i,j] = d[A[i,j]]
	end
end

self = Group("A4",G,A,[1,2,3,4],isEqual::Function)
verifyGroup(self)
printConjugates(self)
printConjugacyClasses(self)















###
