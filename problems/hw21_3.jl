# Steven Schmidt
# Nov 9, 2016
# Phys 601 -- HW21

#Consider two sets, the 4th and 5th roots of unity, combined with
#normal multiplication. In this HW and the next, we'll call them V4
#and V5.
#(a)	Show that each of these sets (under the operation of multiplication) are groups.
#(b)	Find a set of generators for each group and show that it generates the entire group.

#In[4]:=
#Solve[z^4 == 1, z]
#Out[4]= {{z -> -1}, {z -> -I}, {z -> I}, {z -> 1}}
#In[3]:= Solve[z^5 == 1, z]
#Out[3]= {{z -> 1}, {z -> -(-1)^(1/5)}, {z -> (-1)^(
#   2/5)}, {z -> -(-1)^(3/5)}, {z -> (-1)^(4/5)}}


include("./ssgrouputils.jl")
using SSGroupUtils
import SSGroupUtils.printMatrix
import SSGroupUtils.printComplexMatrix
import SSGroupUtils.hasclosure
import SSGroupUtils.Group
import SSGroupUtils.printCayleyTable
import SSGroupUtils.multiply
import SSGroupUtils.inverse
import SSGroupUtils.conjugates
import SSGroupUtils.subgroups
import SSGroupUtils.right_coset
import SSGroupUtils.left_coset
import SSGroupUtils.printInverses
import SSGroupUtils.verifyIdentity
import SSGroupUtils.generate

v4 = zeros(Complex128,4,1)
v4[:] = [1+0im, -1+0im, im, -im]
v5 = zeros(Complex128,5,1)
v5[:] = [1+0im, -(-1+0im)^(1/5), (-1+0im)^(2/5), -(-1+0im)^(3/5), (-1+0im)^(4/5)]

println("v4 = ")
printComplexMatrix(v4)
println("v5 = ")
printComplexMatrix(v5)

v4_cayley = v4*transpose(v4)
v5_cayley = v5*transpose(v5)

println("")
println("v4 cayley table:")
printComplexMatrix(v4_cayley)

println("")
println("v5 cayley table:")
printComplexMatrix(v5_cayley)

function isEqual(a,b)
    if(abs(a-b) < 1e-14 )
        return true
    end
    return false
end

Gv4 = Group("Gv4",v4,v4_cayley,1+0im,isEqual)
Gv5 = Group("Gv5",v5,v5_cayley,1+0im,isEqual)

println("")
println("")
printInverses(Gv4)
println("")
verifyIdentity(Gv4)

println("")
println("")
printInverses(Gv5)
println("")
verifyIdentity(Gv5)

println("")
generating_set = [Gv4.elements_array[2],Gv4.elements_array[3]]
gen_result = generate(Gv4,generating_set)
println("Generating set for Group Gv4:")
@show generating_set
@show gen_result

println("")
generating_set = [Gv5.elements_array[2],Gv5.elements_array[5]]
gen_result = generate(Gv5,generating_set)
println("Generating set for Group Gv5:")
@show generating_set
@show gen_result
