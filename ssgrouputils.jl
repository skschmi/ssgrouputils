# Steven Schmidt
# Phys 601
# HW 21
# Nov, 2016

module SSGroupUtils

function valuesForKeys(dict,array_of_keys)
    result = Array{Any}(length(array_of_keys))
    for (i,key) in enumerate(array_of_keys)
        result[i] = dict[key]
    end
    return result
end

function printMatrix(M)
    print("[")
    for i = 1:size(M)[1]
        if(i>1)
            print(" [")
        else
            print("[")
        end
        for j = 1:size(M)[2]
            print(string(M[i,j]))
            if(j<size(M)[2])
                print(" ")
            else
                print("]")
            end
        end
        if(i < size(M)[1])
            print("\n")
        end
    end
    print("]\n")
end


function printComplexMatrix(M)
    print("[")
    for i = 1:size(M)[1]
        if(i>1)
            print(" [")
        else
            print("[")
        end
        for j = 1:size(M)[2]

            if(imag(M[i,j])<0.0)
                if(real(M[i,j])<0)
                    @printf("%2.4f%2.4fim",real(M[i,j]),imag(M[i,j]))
                else
                    @printf(" %2.4f%2.4fim",real(M[i,j]),imag(M[i,j]))
                end
            elseif(imag(M[i,j])==0.0)
                if(real(M[i,j])<0)
                    @printf("%2.4f+%2.4fim",real(M[i,j]),0.0)
                else
                    @printf(" %2.4f+%2.4fim",real(M[i,j]),0.0)
                end
            else
                if(real(M[i,j])<0)
                    @printf("%2.4f+%2.4fim",real(M[i,j]),imag(M[i,j]))
                else
                    @printf(" %2.4f+%2.4fim",real(M[i,j]),imag(M[i,j]))
                end
            end
            if(j<size(M)[2])
                print("   ")
            else
                print("]")
            end
        end
        if(i < size(M)[1])
            print("\n")
        end
    end
    print("]\n")
end

function hasclosure(potentialsubgroup,cayley)
	for j in cayley
		jfound = false
		for i in potentialsubgroup
			if j == i
				jfound = true
			end
		end
		if(jfound==false)
			return false
		end
	end
	return true
end

function hasrepeats(potentialgroup)
	G = potentialgroup
	d = Dict{Any,Any}()
	for i = 1:length(G)
		d[G[i]] = i
	end
	return length(G) == length(d)
end


type Group
    name::String{}
    elements_array::Array{Any}
    elements_dict::Dict{}
    elements_i::Dict{}
    cayley::Array{}
    identity::Any{}
    inverses::Dict{}
    N::Int64
    isEqual::Function
    function Group(name,elements,caylay,identity,isEqual::Function)
        self = new()
        self.name = name
        self.elements_array = copy(elements)
        self.elements_dict = Dict()
        self.elements_i = Dict()
        self.isEqual = isEqual
        for (i,v) in enumerate(self.elements_array)
            self.elements_dict[i] = v
            self.elements_i[v] = i
        end
        self.inverses = Dict()
        # clean-up the caylay table
        self.cayley = Array{Any}(size(caylay))
        for i = 1:size(caylay)[1]
            for j = 1:size(caylay)[2]
                # Find the element in the 'elements' array using 'isEquals'
                #  and put it in
                for v in self.elements_array
                    if(isEqual(caylay[i,j],v))
                        self.cayley[i,j] = v
                        break
                    end
                end
            end
        end
        self.identity = identity
        self.N = length(self.elements_array)
        for (i,gi) in self.elements_dict
            for (j,gj) in self.elements_dict
                ans = multiply(self,gi,gj)
                if(isEqual(ans,identity))
                    self.inverses[gi] = gj
                end
            end
        end
        return self
    end
end
function printCayleyTable(self,isComplex=false)
    if(isComplex)
        printComplexMatrix(self.cayley)
    else
        printMatrix(self.cayley)
    end
end
function multiply(self,a,b)
    return self.cayley[self.elements_i[a],self.elements_i[b]]
end
function inverse(self,a)
    return self.inverses[a]
end
function are_conjugate(self,a,b)
    result = false
    for g in self.elements_array
        if( self.isEqual(a,multiply(self,multiply(self,g,b),inverse(self,g))) )
            result = true
        end
    end
    return result
end
function conjugates(self,x)
    result = Array{Any}(self.N)
    for (i,g) in self.elements_dict
        result[i] = multiply(self,multiply(self,g,x),inverse(self,g))
    end
    return Set(result)
end
function printConjugates(self)
	println("Conjugates of elements in group ",self.name,":")
	for (i,g) in self.elements_dict
		cj_cl = Set(conjugates(self,g))
		println("  conjugates of ",g,": ",cj_cl)
	end
end
function conjugacy_class_of(self,g)
	conj_class = Set{Any}()
    for g1 in self.elements_array
        if(are_conjugate(self,g,g1))
            push!(conj_class,g1)
        end
    end
    old_len = -1
    while(length(conj_class) != old_len)
        conj_class_copy = copy(conj_class)
        old_len = length(conj_class_copy)
        for gg in conj_class_copy
            for g1 in self.elements_array
                if(are_conjugate(self,gg,g1))
                    push!(conj_class,g1)
                end
            end
        end
    end
	return conj_class
end
function conjugacy_classes(self)
	conj_classes = Set()
	for (i,g) in self.elements_dict
		cj_cl = conjugacy_class_of(self,g)
		push!(conj_classes,cj_cl)
	end
	return conj_classes
end
function printConjugacyClasses(self)
	conj_classes = conjugacy_classes(self)
	println("Conjugacy Classes of group ",self.name,":")
	println("  "*replace(string(conj_classes),",Set","\n  Set"))
end
function center(self)
    result = Array{Any}(0)
    for (key1,value) in self.elements_dict
        trueforall = true
        for (key2,othervalue) in self.elements_dict
            if( !self.isEqual(multiply(self,value,othervalue),multiply(self,othervalue,value)) )
                trueforall = false
            end
        end
        if(trueforall)
            push!(result,value)
        end
    end
    return Set(result)
end
function subgroups(self)
    subg = Array{Any}(0)
    # order-1 subgroups
    indexset = Set()
    for i = 1:self.N
        push!(indexset,Set([i]))
    end
    for iset in indexset
        i = collect(iset)
        potentialsubgroup = valuesForKeys( self.elements_dict, [i[1]] )
        cayley = self.cayley[[i[1]],[i[1]]]
        if(hasclosure(potentialsubgroup,cayley))
            push!(subg,(potentialsubgroup,cayley))
        end
    end
    # order-2 subgroups
    indexset = Set()
    for i = 1:self.N
        for j = 1:self.N
            if(i!=j)
                push!(indexset,Set([i,j]))
            end
        end
    end
    for ijset in indexset
        ij = collect(ijset)
        potentialsubgroup = valuesForKeys( self.elements_dict, [ij[1],ij[2]] )
   		cayley = self.cayley[[ij[1],ij[2]],[ij[1],ij[2]]]
   		if(hasclosure(potentialsubgroup,cayley))
            push!(subg,(potentialsubgroup,cayley))
   		end
    end
    # order-3 subgroups
    indexset = Set()
    for i = 1:self.N
       	for j = 1:self.N
    	   	for k = 1:self.N
                if(i!=j && j!=k && k!=i)
                    push!(indexset,Set([i,j,k]))
                end
            end
        end
    end
	for ijkset in indexset
        ijk = collect(ijkset)
        potentialsubgroup = valuesForKeys( self.elements_dict, [ijk[1],ijk[2],ijk[3]] )
   		cayley = self.cayley[[ijk[1],ijk[2],ijk[3]],[ijk[1],ijk[2],ijk[3]]]
   		if(hasclosure(potentialsubgroup,cayley))
            push!(subg,(potentialsubgroup,cayley))
   		end
    end
    # order-4 subgroups
    indexset = Set()
    for i = 1:self.N
       	for j = 1:self.N
    	   	for k = 1:self.N
                for l = 1:self.N
                    if(i!=j && j!=k && k!=i && l!=i && l!=j && l!=k)
                        push!(indexset,Set([i,j,k,l]))
                    end
                end
            end
        end
    end
	for ijklset in indexset
        ijkl = collect(ijklset)
        potentialsubgroup = valuesForKeys( self.elements_dict, [ijkl[1],ijkl[2],ijkl[3],ijkl[4]] )
   		cayley = self.cayley[[ijkl[1],ijkl[2],ijkl[3],ijkl[4]],[ijkl[1],ijkl[2],ijkl[3],ijkl[4]]]
   		if(hasclosure(potentialsubgroup,cayley))
            push!(subg,(potentialsubgroup,cayley))
   		end
    end
    return subg
end
function right_coset(self,subgroup,t)
    coset = Array{Any}(length(subgroup))
    for (i,a) in enumerate(subgroup)
        coset[i] = multiply(self,a,t)
    end
    return Set(coset)
end
function left_coset(self,subgroup,t)
    coset = Array{Any}(length(subgroup))
    for (i,a) in enumerate(subgroup)
        coset[i] = multiply(self,t,a)
    end
    return Set(coset)
end
function generate(self,elements)
    generated_set = Set(elements)
    old_len = -1
    while(old_len != length(generated_set))
        old_len = length(generated_set)
        for v1 in generated_set
            for v2 in generated_set
            	newval = multiply(self,v1,v2)
                push!(generated_set,newval)
            end
        end
    end
    return generated_set
end
function verifyIdentity(self)
    println(self.name*": Verifying identity:")
    all_passed = true
    for (i,g) in self.elements_dict
        RESULT = self.isEqual(multiply(self,g,self.identity),g) && self.isEqual(multiply(self,self.identity,g),g)
        println("identity * ("*string(g)*") = ("*string(g)*") * identity = ("*string(string(g))*"):  "*string(RESULT))
        if(!RESULT)
            all_passed = false
        end
    end
    if(all_passed)
        println("Success - Group '"*self.name*"' has valid identity")
    else
        println("Failed - Identity test failed. '"*self.name*"' is not a valid group.")
    end
    return all_passed
end
function verifyGroupElementsHaveNoRepeats(self)
	println(self.name*": Verifying group elements have no repeats:")
	all_passed = true
	if( length(self.elements_dict) == size(self.cayley)[1] &&
	    size(self.cayley)[1] == size(self.cayley)[2] )
		println("Success - Group '"*self.name*"' has no repeats")
    else
        println("Failed - Repeats found in group elements. '"*self.name*"' is not a valid group.")
        all_passed = false
    end
    return all_passed
end
function verifyGroupCayleyIsValid(self)
	println(self.name*": Verifying group has valid cayley table:")
	all_passed = true
	if( hasclosure(self.elements_array,self.cayley) )
		println("Success - Group '"*self.name*"' has valid cayley table")
	else
        println("Failed - Cayley table is not closed. '"*self.name*"' is not a valid group.")
        all_passed = false
    end
    return all_passed
end
function verifyGroup(self)
	println(self.name*": Verifying group...")
	all_passed = true
	if( !verifyIdentity(self) )
		all_passed = false
	end
	if( !verifyGroupElementsHaveNoRepeats(self) )
		all_passed = false
	end
	if( !verifyGroupCayleyIsValid(self) )
		all_passed = false
	end
	if( all_passed )
		println("Success - Group '"*self.name*"' is a valid group!")
	else
        println("Failed! '"*self.name*"' is not a valid group.")
    end
end
function printInverses(self)
    println(self.name*" inverses:")
    for (key,value) in self.inverses
        println("  "*string(key)*"  ->  "*string(value))
    end
end
function printCosetsForSubgroup(self,subgroup)
    println("Cosets for subgroup "*string(subgroup)*":")
    sg = subgroup
    all_r_cosets = Set()
    all_l_cosets = Set()
    for i=1:length(self.elements_dict)
        r_coset = right_coset(self,sg,self.elements_dict[i])
        l_coset = left_coset(self,sg,self.elements_dict[i])
        push!(all_r_cosets,r_coset)
        push!(all_l_cosets,l_coset)
        print("Right coset (S*t) for t=",self.elements_dict[i],": ",r_coset)
        print("  Left coset (t*S) for t=",self.elements_dict[i],": ",l_coset)
        print("\n")
    end
    print("All right cosets for subgroup: ")
    println(all_r_cosets)
    print("All left cosets for subgroup:  ")
    println(all_l_cosets)
    println("")
end
function printSubgroups(self)
    subg = subgroups(self)
    println("")
    println("Subgroups of "*self.name*" group: ")
    for (sg,scay) in subg
        print(string(sg)*":\n")
        printMatrix(scay)
        printCosetsForSubgroup(self,sg)
    end
end
function printCenter(self)
    thecenter = center(self)
    println("")
    println("Center of group "*self.name*": ")
    println(thecenter)
    println("")
end
function printGeneratedFrom(self,elements)
    generated = generate(self,elements)
    println("Set generated by "*string(elements)*":")
    println("Generated: ", generated)
    println("")
end

function generateRegularRepresentation(self)
	n = self.N
	R = Array{Array{Float64,2}}(n)
	for g_i = 1:length(self.elements_dict)
		R[g_i] = zeros(n,n)
		g = self.elements_dict[g_i]
		for (g1_i,g1) in self.elements_dict
			g2 = multiply(self,g,g1)
			g2_i = self.elements_i[g2]
			R[g_i][g2_i,g1_i] = 1
		end
	end
	return R
end

function permute(permutation,v)
	w = copy(v)
	for i = 1:length(v)
		w[i] = v[permutation[i]]
	end
	return w
end
function cayleyTableFromPermutationGroup(G)
	# computes a cayley table from G, assuming G is made up of
	# arrays of integers which represent the permutation from
	# [1,2,3,...n]
	cayley = Array{Any,2}(length(G),length(G))
	for i = 1:length(G)
		for j = 1:length(G)
			cayley[i,j] = permute(G[i],G[j])
		end
	end
	return cayley
end


end # module SSGroupUtils
